import * as L from 'leaflet';
import {Park} from './park'
import {IFilter} from './filter'
import {ParkDetailsLink} from './app.component'

export class NRVMap {
    public Map: L.Map;
    public Layer: L.TileLayer;
    public Markers: Array<L.marker> = [];
    public UserLocation: L.marker;
    public IndexedMarkers: {[key: number]: L.marker} = {};
    public Filter: IFilter = null;

    constructor(selector: string, layerURL: string, attribution: string, lat: number, lon: number) {
        this.Map = new L.Map(selector).setView([lat, lon], 10);
        this.Layer = new L.TileLayer(layerURL, { attribution: attribution });
        this.Layer.addTo(this.Map);
    }

    public AddPark(park: Park) {
        this.IndexedMarkers[park.Id] = new L.marker([park.Lat, park.Lon]).addTo(this.Map);
        
        let parkDescription = '';
        if (park.Description != '') {
            parkDescription = "<small>" + park.Description + "</small>";
        }

        // cheating - consider generating component
        var popup = L.popup({
            autoPanPaddingTopLeft: [0, 100]
        }).setContent("<h2>" + park.Name + "</h2><h2>" + parkDescription + 
            "<button class='disable-hover button button-ios button-default button-default-ios button-block button-block-ios' href='#' onclick=\"document.getElementById('toggleRightMenu' + "+park.Id+").click()\">Details</button><h2>");

        this.IndexedMarkers[park.Id].bindPopup(popup);

        this.Markers.push(this.IndexedMarkers[park.Id]);
    }

    public HideMarker(parkId: number) {
        this.IndexedMarkers[parkId].remove();
    }

    public ShowMarker(parkId: number) {
        this.IndexedMarkers[parkId].addTo(this.Map);
    }

    public FitBounds() {
        var visibleMarkers = this.Markers.filter(marker => marker._map != null);
        var group = new L.featureGroup(visibleMarkers);
        if(typeof(this.UserLocation) !== 'undefined') {
            group.addLayer(this.UserLocation);
        }
        if(visibleMarkers.length > 1) {
            this.Map.fitBounds(group.getBounds(), {paddingTopLeft: [0, 100]});   
        }
    }

    public SetUserLocation(position) {
        var currentIcon = L.icon({
            iconUrl: 'assets/imgs/current.svg',
            iconSize:     [38, 38]
        });

        this.UserLocation = new L.marker([position.coords.latitude, position.coords.longitude], {icon: currentIcon}).addTo(this.Map);
        this.UserLocation.setZIndexOffset(100);

        var popup = L.popup({
            autoPanPaddingTopLeft: [0, 100]
        }).setContent("<h2>You</h2>");
        
        this.UserLocation.bindPopup(popup);
        this.Map.setView([position.coords.latitude, position.coords.longitude], 14);
    }
}