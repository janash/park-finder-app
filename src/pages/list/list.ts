import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { IData } from '../../app/data';
import { Park } from '../../app/park';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  public Data: IData;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.Data = navParams.get('data');
  }

  SetActivePark(park: Park) {
    this.Data.ActivePark = park;
  }
}
